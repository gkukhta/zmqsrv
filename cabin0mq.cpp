#include <iostream>
#include <zmq.hpp>
#include "cabin0mq.hpp"

cabin0mq::cabin0mq(const string& bindAddr, const string& connectAddr) :
    repRunning(true),
    processingRunning(true),
    canContinue(true),
    totalRetries(0),
    repeatedCmds(0),
    reqSeqNr(0),
    serverAddr(bindAddr),
    clientAddr(connectAddr)
{
    serverThread = make_unique<thread> ([this] {repServer();});
    processingThread = make_unique<thread> ([this] {processQ();});
    clientThread = make_unique<thread> ([this] {reqClient();});
}

cabin0mq::~cabin0mq()
{
    canContinue = false;
    cout << "IS Waiting for " << serverAddr << endl;
    serverThread->join();
    cout << "QUEUE Waiting for " << serverAddr << endl;
    processingThread->join();
    cout << "IC Waiting for " << clientAddr << endl;
    clientThread->join();
}

inline static string receiveStr (zmq::socket_t& socket)
{
    zmq::message_t message;
    socket.recv(message);
    return string(static_cast<char*>(message.data()), message.size());
}

inline static void sendStr (zmq::socket_t& socket, string msg)
{
    socket.send(zmq::message_t(static_cast<const void*>(msg.data()), msg.size()), zmq::send_flags::dontwait);
}

template<typename ... Args>
inline string string_format( const string& format, Args ... args )
{
    size_t size = snprintf( nullptr, 0, format.c_str(), args ... ) + 1; // Extra space for '\0'
    if( size <= 0 ){ throw std::runtime_error( "Error during formatting." ); }
    unique_ptr<char[]> buf( new char[ size ] );
    snprintf( buf.get(), size, format.c_str(), args ... );
    return string( buf.get(), buf.get() + size - 1 ); // We don't want the '\0' inside
}

void cabin0mq::repServer()
{
    zmq::context_t context(1);
    zmq::socket_t server(context, ZMQ_REP);
    server.bind(serverAddr);
    cout << "IS Started " << serverAddr << endl;
    int prevSeqNr;
    int seqNr {-1};
    while (canContinue)
    {
        zmq::pollitem_t items[] = {
            { static_cast<void*>(server), 0, ZMQ_POLLIN, 0 } };
        zmq::poll (&items[0], 1, 500); //ждать 500мс.
        if (items[0].revents & ZMQ_POLLIN)
        {
            auto allCmd = receiveStr(server);
            prevSeqNr = seqNr;
            seqNr = stoi(allCmd.substr(0, 5));
            auto cmd = allCmd.substr(5);
            sendStr (server, cmd);
            cout << "REP " << serverAddr <<" >>>> " << allCmd << endl;
            if (prevSeqNr != seqNr)
            {
                inQ.push(cmd);
                semInQ.notify();
            } else
            {
                cout << "!!! Повторение команды !!!\n";
                ++repeatedCmds;
            }
        }
    }
    repRunning = false;
    semInQ.notify();
    cout << "REP " << serverAddr << " finished" << endl;
}

void cabin0mq::processQ()
{
    while(repRunning)
    {
        semInQ.wait();
        if (!inQ.empty())
        {
            auto cmdFromQ = inQ.front();
            inQ.pop();
            outQ.push(processCmd(cmdFromQ));
            semOutQ.notify();
        }
    }
    //process queue before exit
    while(!inQ.empty()) {
        auto cmdFromQ = inQ.front();
        inQ.pop();
        outQ.push(processCmd(cmdFromQ));
        semOutQ.notify();
    }
    processingRunning=false;
    semOutQ.notify();
    cout << "QUEUE " << serverAddr << " finished" << endl;
}

inline string cabin0mq::getResultFromQ()
{
    auto cmdResult = outQ.front();
    outQ.pop();
    reqSeqNr=(++reqSeqNr)%30000;
    return string_format("%5u", reqSeqNr) + cmdResult;
}

void cabin0mq::reqClient()
{
    constexpr auto requestTimeout {3000};
    const int linger {0};
    zmq::context_t context (1);
    auto client = make_unique<zmq::socket_t>(context, ZMQ_REQ);
    cout << "IC " << clientAddr <<": connecting..." << endl;
    client->connect (clientAddr);
    cout << "IC " << clientAddr << ": CONNECTED"  << endl;
    //  Configure socket to not wait at close time
    client->setsockopt (ZMQ_LINGER, &linger, sizeof (linger));
    while (processingRunning)
    {
        semOutQ.wait();
        if (!outQ.empty())
        {
            auto cmdResult = getResultFromQ();
            sendStr (*client, cmdResult);
            cout << "REQ " << clientAddr << " <<<< " << cmdResult << endl;
            bool expect_reply = true;
            while (expect_reply)
            {
                //  Poll socket for a reply, with timeout
                zmq::pollitem_t items[] = {
                    { static_cast<void*>(*client), 0, ZMQ_POLLIN, 0 } };
                zmq::poll (&items[0], 1, requestTimeout);
                if (items[0].revents & ZMQ_POLLIN)
                {
                    //  We got a reply from the server, must match sequence
                    receiveStr (*client);
                    expect_reply = false;
                }
                else if (processingRunning)
                {
                    ++totalRetries;
                    cout << "W: no response from server, retrying..." << endl;
                    //  Old socket will be confused; close it and open a new one
                    client.reset(new zmq::socket_t(context,ZMQ_REQ));
                    cout << "IC " << clientAddr <<": connecting..." << endl;
                    client->connect (clientAddr);
                    cout << "IC " << clientAddr << ": CONNECTED"  << endl;
                    //  Configure socket to not wait at close time
                    client->setsockopt (ZMQ_LINGER, &linger, sizeof (linger));
                    //  Send request again, on new socket
                    sendStr (*client, cmdResult);
                    cout << "REQ " << clientAddr << " <<<< " << cmdResult << endl;
                } else //клиент не отвечает, завершаем поток
                {
                    cout << "Client not responding. REQ " << clientAddr << " finished" << endl;
                    return;
                }
            }
        }
    }
    //try to send all results to all alive clients before exit
    while(!outQ.empty())
    {
        auto cmdResult = getResultFromQ();
        sendStr (*client, cmdResult);
        cout << "Final REQ " << clientAddr << " <<<< " << cmdResult << endl;
        zmq::pollitem_t items[] = {
            { static_cast<void*>(*client), 0, ZMQ_POLLIN, 0 } };
        zmq::poll (&items[0], 1, requestTimeout);
        if (items[0].revents & ZMQ_POLLIN)
            receiveStr (*client);
        else
        {
            cout << "Client not responding. REQ " << clientAddr << " finished" << endl;
            return;
        }
    }
    cout << "REQ " << clientAddr << " finished" << endl;
}
