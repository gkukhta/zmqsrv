#include <iostream>
#include "cabin0mq.hpp"

using namespace std;

int main(int argc, char *argv[])
// parameters: clientAddr serverport clientport
{
    if (argc == 4)
    {
        const string serverAddr {"tcp://*:" + string(argv[2])};
        const string clientAddr {"tcp://" + string(argv[1]) + ":" + string(argv[3])};
        auto cabinPtr = make_unique<cabin0mq> (serverAddr, clientAddr);
        cout << "<Enter> для завершения" << endl;
        getc(stdin);
        cout << "Total reconnects: " << cabinPtr->getRetries() << endl;
        cout << "Повторов команд: " << cabinPtr->getRepeatedCmds() << endl;
    } else
        cout << "Usage: " << "zmqsrv clientAddr serverport clientport" << endl;
    return 0;
}
