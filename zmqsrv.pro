TEMPLATE = app
CONFIG += c++17 console link_pkgconfig
CONFIG -= app_bundle
CONFIG -= qt
PKGCONFIG += libzmq

SOURCES += \
        cabin0mq.cpp \
        main.cpp \
        processcmd.cpp

HEADERS += \
    atomic_queue.hpp \
    cabin0mq.hpp \
    semaphore.hpp

LIBS = -lpthread
