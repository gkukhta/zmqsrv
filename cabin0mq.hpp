#pragma once
#include <atomic>
#include <thread>
#include "atomic_queue.hpp"
#include "semaphore.hpp"

using namespace std;

string processCmd(const string& cmd);

class cabin0mq
{
public:
    cabin0mq(const string& bindAddr, const string& connectAddr);
    ~cabin0mq();
    ulong getRetries() const {return totalRetries;};
    ulong getRepeatedCmds() const {return repeatedCmds;};
    cabin0mq(const cabin0mq&) = delete;
    cabin0mq& operator=(const cabin0mq&) = delete;
private:
    atomic_bool repRunning;
    atomic_bool processingRunning;
    atomic_bool canContinue;
    atomic_ulong totalRetries;
    atomic_ulong repeatedCmds;
    int reqSeqNr;
    const string& serverAddr;
    const string& clientAddr;
    atomic_queue<string> inQ;
    atomic_queue<string> outQ;
    semaphore semInQ;
    semaphore semOutQ;
    unique_ptr<thread> serverThread;
    unique_ptr<thread> processingThread;
    unique_ptr<thread> clientThread;
    void repServer();
    void processQ();
    void reqClient();
    string getResultFromQ();
};

